import json
import requests
from bs4 import BeautifulSoup

url = "https://coinmarketcap.com/currencies/bitcoin/#markets"

MINIUM_VOLUMN = 100000
PAIR_FILTER = ['/KRW', '/USDT', 'ETH/']

def filter_data(price_list):
    print("filter data")
    filter_list = {}
    for item in price_list:
        if item.get('updated') != 'Recently':
            continue

        if item.get('volumn') < MINIUM_VOLUMN:
            continue
        
        has_pair = False
        for pair_item in PAIR_FILTER:
            if pair_item in item.get('pair'):
                has_pair = True
                break 
    
        if False == has_pair:
            continue

        if item.get('pair') in filter_list:
            filter_list[item.get('pair')].append(item)
        else:
            filter_list.update({item.get('pair') : [item]})
    return filter_list

def analyze_data(filter_list):
    print("analyze")

    result = {}

    for key, items in filter_list.items():
        min_item = None
        max_item = None        

        for item in items:
            if min_item == None:
                min_item = item
                max_item = item
                continue

            price = item.get('price')
            if min_item.get('price') > price:
                min_item = item
                continue

            if max_item.get('price') < price:
                max_item = item
                continue
        
        result.update({
            key : {
                'min' : min_item,
                'max' : max_item,
                'gap_price' : round(max_item.get('price') - min_item.get('price'), 1),
                'gap_percent' : round(100 - (min_item.get('price') / max_item.get('price') * 100), 1) 
            }
        })

    return result

if __name__ == "__main__":
    print("request url : " + url)

    r = requests.get(url)
    soup = BeautifulSoup(r.content, 'html.parser')

    # parse coin market price
    result_lists = []
    rows = soup.find(id='markets-table').find('tbody').find_all('tr')
    for row in rows:
        tds = row.find_all('td')
        item = {
            'name' : tds[1].find('a').get_text(),
            'pair' : tds[2].find('a').get_text(),
            'volumn' : float(tds[3].find('span')['data-usd']),
            'price' : float(tds[4].find('span')['data-usd']),
            'updated' : tds[6].get_text()
        }
        result_lists.append(item)
    
    filtered_data = filter_data(result_lists)
    analyzed_data = analyze_data(filtered_data)
    with open('analyze_price.json', 'w') as fp:
        json.dump(analyzed_data, fp)


